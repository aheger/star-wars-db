import gql from "graphql-tag";

export const SEARCH_HUMANS = gql`
  query SearchHumans($nameFilter: String!) {
    allPersons(
      filter: { species_every: { name: "Human" }, name_contains: $nameFilter },
      orderBy: name_ASC
    ) {
      id
      name
    }
  }
`;

export const GET_PERSON = gql`
  query GetPerson($id: ID!) {
    person: Person(id: $id) {
      id
      birthYear
      eyeColor
      films(orderBy: releaseDate_ASC) {
        id
        releaseDate
        title
      }
      gender
      hairColor
      height
      homeworld {
        id
        name
      }
      mass
      name
      species {
        id
        name
      }
      skinColor
    }
  }
`;

export const GET_FILM_HUMANS = gql`
  query FilmHumans {
    charactersByFilm: allFilms(orderBy: releaseDate_ASC) {
      id
      title
      characters(
        filter: { species_every: { name: "Human" } }
      ) {
        id
      }
    }
    allCharacters: allPersons(
      filter: { species_every: { name: "Human" } }
    ) {
      id
      name
    }
  }
`;
