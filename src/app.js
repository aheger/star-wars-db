import React from "react";

import { ApolloProvider } from "react-apollo";
import { BrowserRouter } from "react-router-dom";
import { Provider as ReduxProvider } from "react-redux";

import Header from "./header";
import PageSwitcher from "./page-switcher";

import apiClient from "./api/client";
import store from "./store/store";

export default () => {
  return (
    <ApolloProvider client={apiClient}>
      <ReduxProvider store={store}>
        <BrowserRouter basename={process.env.PUBLIC_URL}>
          <div className="app">
            <Header />
            <PageSwitcher />
          </div>
        </BrowserRouter>
      </ReduxProvider>
    </ApolloProvider>
  );
}
