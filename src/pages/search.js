import React, { useState, useRef } from "react";

import HumanSearchLoader from "../components/human-search/loader";

// TODO user should be able to navigate back to previous search result

export default function Search() {
  const [searchTerm, setSearchTerm] = useState(null);
  const searchInput = useRef(null);

  const handleSubmit = (e) => {
    e.preventDefault();
    setSearchTerm(searchInput.current.value);
  }

  return (
    <div className="page">
      <form className="search-form" onSubmit={handleSubmit}>
        <input
          type="search"
          placeholder="Search for humans"
          ref={searchInput}
          autoFocus
        />
        <input type="submit" value="Search" />
      </form>
      {searchTerm &&
          <HumanSearchLoader searchTerm={searchTerm} />
      }
    </div>
  );
}
