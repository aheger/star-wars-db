import React from "react";
import { Query } from "react-apollo";

import { GET_PERSON } from "../api/queries";

import ErrorMessage from "../components/error-message";
import HumanFilmCompanions from "../components/person/human-film-companions/human-film-companions";
import PersonDetails from "../components/person/details/details";
import PersonHistoryAppender from "../components/person/history-appender";
import Spinner from "../components/spinner";

export default ({ match: { params: { id }}}) => (
  <div className="page">
    <Query query={GET_PERSON} variables={{ id }}>
    {({ data, loading, error }) => {
      if (loading) return <Spinner />;
      if (error) return <ErrorMessage error={error} />;

      const { person } = data;

      if (!person) return <ErrorMessage error={new Error("Unknown person")} />

      return (
        <>
          <PersonDetails person={person} />
          <HumanFilmCompanions person={person} />
          <PersonHistoryAppender person={person} />
        </>
      );
    }}
    </Query>
  </div>
);
