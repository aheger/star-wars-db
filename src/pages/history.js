import React from "react";
import { connect } from "react-redux";

import PersonList from "../components/person/list";

const mapStateToProps = ({ history }) => {
  return { history };
};

const personHistory = ({ history }) => {
  return (
    <div className="page">
    { history.length
      ? <PersonList persons={history} />
      : <div className="text-center text-muted">The history is empty</div>
    }
    </div>
  );
}

export default connect(mapStateToProps)(personHistory);
