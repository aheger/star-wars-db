import React from "react";
import { connect } from "react-redux";
import { NavLink, withRouter } from "react-router-dom";

const mapStateToProps = ({ history: { length } }, ownProps) => {
  return {
    historyLength: length
  };
};

const Header = ({ historyLength }) => {
  return (
    <header>
      <nav>
        <NavLink exact to="/" activeClassName="is-active">Search</NavLink>
        <NavLink to="/history" activeClassName="is-active">History
        { historyLength
          ? <> <span className="badge">{ historyLength }</span></>
          : null
        }
        </NavLink>
      </nav>
    </header>
  );
}

export default withRouter(connect(mapStateToProps)(Header));
