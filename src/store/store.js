import { createStore } from "redux";

import { ACTIONS } from "./actions";

const defaultState = {
  history: []
};

const reducer = (state = defaultState, action) => {
  const { payLoad } = action;

  switch(action.type) {
    case ACTIONS.ADD_HISTORY:
      // remove duplicate history entry
      const newHistory = state.history.filter(person => person.id !== payLoad.id);

      newHistory.unshift(payLoad);

      return {
        ...state,
        history: newHistory
      };

    default: return state;
  }
}

export default createStore(reducer);
