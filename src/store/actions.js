const ACTIONS = {
  ADD_HISTORY: "ADD_HISTORY",
};

export { ACTIONS };

export const addHistory = (entry) => ({
  type: ACTIONS.ADD_HISTORY,
  payLoad: entry,
});

