import React from "react";

import { Route, Switch } from "react-router";

import HistoryPage from "./pages/history";
import PersonPage from "./pages/person";
import SearchPage from "./pages/search";

export default () => {
  return (
    <div className="main">
      <Switch>
        <Route exact path="/" component={SearchPage} />
        <Route path="/history" component={HistoryPage} />
        <Route path="/person/:id" component={PersonPage} />
      </Switch>
    </div>
  );
};
