import React from "react";
import { Query } from "react-apollo";

import { SEARCH_HUMANS } from "../../api/queries";

import ErrorMessage from "../error-message";
import PersonList from "../person/list";
import Spinner from "../spinner";

export default ({ searchTerm }) => {
  //TODO extract query result handling into function/component
  return (
    <Query query={SEARCH_HUMANS} variables={{ nameFilter: searchTerm }}>
    {({ data, loading, error }) => {
      if (loading) return <Spinner />;
      if (error) return <ErrorMessage error={error} />;

      const { allPersons: humans } = data;

      if (!humans || !humans.length) return (
        <div className="text-muted text-center">No humans found</div>
      );

      return (
        <PersonList persons={humans}/>
      );
    }}
    </Query>
  );
}

