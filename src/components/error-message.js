import React from "react";

export default ({ error = {}}) => {
  return (
    <div className="card error-message">
      {"Error"}
      <hr />
      <small>
        <div>
          {error.message || JSON.stringify(error)}
        </div>
      </small>
    </div>
  );
}
