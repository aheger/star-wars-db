import { connect } from "react-redux";

import { addHistory } from "../../store/actions";

const historyAppender = ({ dispatch, person }) => {
  dispatch(addHistory(person));

  return null;
}

export default connect()(historyAppender);
