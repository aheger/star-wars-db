import React from "react";
import { Query } from "react-apollo";

import { GET_FILM_HUMANS } from "../../../api/queries";

import ErrorMessage from "../../../components/error-message";
import PersonList from "../../../components/person/list";
import Spinner from "../../../components/spinner";

import findCompanions from "./find-companions";

export default ({ person }) => (
  <div className="card">
    <Query query={GET_FILM_HUMANS}>
    {({ data, loading, error }) => {
      if (loading) return <Spinner />;
      if (error) return <ErrorMessage error={error} />;

      const { charactersByFilm, allCharacters } = data;

      const companions = findCompanions(
        person.id,
        charactersByFilm,
        allCharacters,
        2
      );

      companions.sort((c1, c2) => c1.name.localeCompare(c2.name));

      return (
        <>
        { companions.length
          ? (<>
              <div className="card-header">{`${person.name}'s companions:`}</div>
              <small>
                <PersonList persons={companions}/>
              </small>
            </>)
          : <span>{`${person.name} has no companions`}</span>
        }
        </>
      );
    }}
    </Query>
  </div>
);

// TODO define propTypes
