// TODO this feels very contrived - is there a cleaner solution with GraphQL?

// Return whether the given {character} appears in {film}
const isCharacterInFilm = (character, film) => {
  return film.characters
    .some((filmCharacter) => filmCharacter.id === character.id);
}

/**
 * Returns whether the given {character} is in {streakLength} consecutive movies
 * @param {Number} streakLength length of streak
 * @param {Person} character character to find
 * @param {Number} filmIndex index of the starting film
 * @param {Film[]} films list of films
 * @returns {Boolean}
 */
const isStreak = (streakLength, character, filmIndex, films) => {
  const filmsLeft = films.length - filmIndex;


  if (streakLength > filmsLeft) {
    // required streak length is smaller than number of movies left to examine
    return false;
  }

  return films
    .slice(filmIndex, filmIndex + streakLength)
    .every(isCharacterInFilm.bind(null, character))
}


/**
 * Find other film characters that play together with the character of
 * {pivotCharacterId} in at least {streakLength} consecutive movies.
 *
 * @param {String} pivotCharacterId id of the pivot character
 * @param {Film[]} charactersByFilm ordered list of movies with their characters
 * @param {Person[]} allCharacters list of all characters
 * @param {Number} streakLength number of consecutive movies the characters must be in together
 * @returns {Person[]} list of film companions
 */
export default (pivotCharacterId, charactersByFilm, allCharacters, streakLength = 2) => {
  if (streakLength < 1) {
    throw new Error("find-companions: streakLength must be >= 1");
  }

  const characterMap = allCharacters.reduce((memo, curr) => {
    memo[curr.id] = curr;
    return memo;
  }, {});

  const pivotCharacter = characterMap[pivotCharacterId];
  if (!pivotCharacter) {
    // unknown character
    return [];
  }

  const companions = new Set();

  const isStreakWithLength = isStreak.bind(null, streakLength);

  for (let iFilm = 0; iFilm < charactersByFilm.length - streakLength; iFilm++) {
    // check if pivot character has a streak in this film
    const pivotCharacterHasStreak = isStreakWithLength(pivotCharacter, iFilm, charactersByFilm);
    if (!pivotCharacterHasStreak) {
      continue;
    }

    const film = charactersByFilm[iFilm];

    // every character that also has a streak in the same film
    // is a companion
    film.characters
      .filter(character => {
        if (!characterMap[character.id]) {
          return false;
        }

        if (character.id === pivotCharacter.id) {
          return false
        }

        return isStreakWithLength(character, iFilm, charactersByFilm);
      })
      .forEach(character => {
        companions.add(character.id);
      });
  }

  return [...companions.values()]
    .map(id => characterMap[id]);
}

