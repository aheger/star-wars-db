import React from "react";
import { NavLink } from "react-router-dom";

export default ({ persons }) => {
  return (
    <div>
    {persons.map(person => (
      <NavLink key={person.id} className="no-decoration" to={`/person/${person.id}`}>
        <div className="card card-hover">
            <span>{person.name}</span>
        </div>
      </NavLink>
    ))}
    </div>
  );
}
