import React from "react";

export default ({ label, value, transform = x => x}) => {
  value = [].concat(value);

  if (!value.length) {
    value = [null];
  }

  return (
    <>
      <dt>{label}</dt>
      {value.map((value, index) => (
        <dd key={index}>{transform(value)}</dd>
      ))}
    </>
  );
}

// TODO define propTypes

