const EMPTY_VALUE = "-";

const normalizeValue = (value) => {
  if (value === null) {
    return EMPTY_VALUE;
  }

  value = String(value).toLowerCase();

  if (value === "unknown") {
    return EMPTY_VALUE;
  }

  return value;
}

export default normalizeValue;
