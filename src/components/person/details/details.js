import React from "react";

import DetailField from "./detail-field";
import normalizeValue from "./normalize-value";
import normalizeFilmValue from "./normalize-film-value";

// append unit to function's return value
const withUnit = (fn, unit) => {
  return (...args) => `${fn(...args)} ${unit}`;
}

// select only the name from an item
const selectName = (fn, item) => {
  return (item) => fn(item ? item.name : null);
}

export default ({ person }) => {
  return (
    <div className="card">
      <div className="card-header">{ person.name }</div>
      <small>
        <dl>
          <DetailField label="Name" value={person.name}/>
          <DetailField label="Species" value={person.species} transform={selectName(normalizeValue)}/>
          <DetailField label="Birth year" value={person.birthYear} transform={normalizeValue}/>
          <DetailField label="Gender" value={person.gender} transform={normalizeValue}/>
          <DetailField label="Height" value={person.height} transform={withUnit(normalizeValue, "cm")}/>
          <DetailField label="Mass" value={person.mass} transform={withUnit(normalizeValue, "kg")}/>
          <DetailField label="Eye color" value={person.eyeColor} transform={normalizeValue}/>
          <DetailField label="Hair color" value={person.hairColor} transform={normalizeValue}/>
          <DetailField label="Skin color" value={person.skinColor} transform={normalizeValue}/>
          <DetailField label="Films" value={person.films} transform={normalizeFilmValue}/>
        </dl>
      </small>
    </div>
  );
}
