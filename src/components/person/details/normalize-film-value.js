export default (film) => {
  return `${new Date(film.releaseDate).getFullYear()} ${film.title}`;
}
