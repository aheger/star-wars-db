import React from "react";

export default () => {
  return (
    <div className="spinner-wrapper">
      <div className="spinner"></div>
    </div>
  );
}
