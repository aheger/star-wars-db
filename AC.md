(x) The application should have three routes.

(x) The first route (/) should display a search bar where users can enter a name.
(x) After submitting the query the page should also display a list with the names
of all __humans__ in the Star Wars database whose names match the entered string.

(x) Each list entry should also be a link to the second route (/person/:id).
(x) This page should display some of the given person's individual
characteristics of your choice.

(x) Bonus task: Display the list of __humans__ who played with this person in at least
two consecutive movies, with the links leading to their profile pages.

(x) The third route (/history) should list all the humans which the user already
has looked at (e.g. visited the detail page) in the current session. You can
use the redux store to keep this information.

(x) On top of all of the routes should be a main navigation menu with links to the
'Search' and 'History' routes.
